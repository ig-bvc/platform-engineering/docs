# Platform Engineering

## Überblick

Konzipierung und Implementierung von Toolchains und Workflows, welche Softwareentwickler und Betrieb in die Lage versetzen, Self-Service durchzuführen.

![](/img/platform_components.png "Quelle: CNCF TAG App Delivery - https://github.com/cncf/tag-app-delivery/blob/platforms-v1alpha1/platforms-whitepaper/v1alpha1/assets/platform_components.png") *Quelle: CNCF TAG App Delivery - Whitepaper Platform Engineering*

Die Produktivität von Mitarbeitern wird erhöht und verbessert den value stream.

Etablierug von Standards, Methoden und Vorgehenweisen.

Deckt den kompletten Lifecycle ab.

Produktzentrierter und kundenorientierter Ansatz.

"Golden Path"

## Unterscheidung zu DevOps

- Nächste Phase nach DevOps
- Platform Engineering ist ein DevOps Ansatz.
- Erhöhung der Produktivität und Experience durch Self Service an einer gemeinsamen Plattform.
- Platform Engineering setzt Tools ein, um Entwicklung, Management und Monitoring durch Automatisierung und Visualisierung zu kanalisieren. Es werden wiederverwendbare Tools und Services über organisatorische Grenzen und Anwendungsfälle angeboten.

# Untescheidung zu SRE

- Ziel von SRE: Reliability
  Platform Engineering beschleunigt den Delivery Prozess. Platform Engineering unterstützt während des ganzen CD Lebenszylus.

# Zusammenfassung

- Ziel SRE: Application Reliability
- Ziel Platform Engineering: Effiziente und schnelle Software Delivery Prozesse
- Ziel DevOps: Optimierung des Entwicklungsprozesses

Speziell Platform Engineering:

- Fortlaufende Sichtbarkeit in Services und Owners.
  - unterstützt z.b. SRE
- Self Serice und Automatisierung mit Tools und Platforms unterstützen Anwender unter Einhaltung von Freiheit und Unabhängigkeit.
- Flexible und Skalierbare Plattformen.

# Beispiele

- Allgemeines Beispiel: Container Images bereitstellen
  Über die Kernaufgabe hinausgehende übergreifende und standardisierte Aspekte:
  - Umgebungsvariablen und andere Konfigurationsquellen
  - Berücksichtigung von Services und Abhängigkeiten
  - Rollback
  - Debugging
  - Umgebungen hochfahren
  - Refactoring
  - Verwaltung von Ressourcen
  - RBAC

- Kubernetes selber
- Crossplane
- IaC
- awx
- nx.dev

# Referenzen

- [CNCF TAG App Delivery - Platforms Whitepaper](https://github.com/cncf/tag-app-delivery/blob/platforms-v1alpha1/platforms-whitepaper/v1alpha1/paper.md)
